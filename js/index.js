$(document).ready(function () {
  $(window).bind("scroll", function () {
    var navHeight = $(window).height() - 70;
    if ($(window).scrollTop() > navHeight) {
      $("nav").addClass("fixed");
    } else {
      $("nav").removeClass("fixed");
    }
  });
});

/* Vue Setup */
var mainApp = new Vue({
  el: "#main-dynamic",
  data: {
    sections: s,
  },
});

var contactApp = new Vue({
  el: "#contact",
  data: {
    sociallinks: l,
  },
});

var resumeApp = new Vue({
  el: "#resumne-contact",
  data: {
    sociallinks: l,
  },
});

/* initial animation */
var name = "Cunningham Development";
var counter1 = -1;
function addTopText() {
  setTimeout(function () {
    if (counter1++ < name.length) {
      $("#top-name").append(name[counter1]);
      addTopText();
    }
  }, 100);
}

var headline = "I'm John Cunningham.";
var counter2 = -1;
function addHeadText() {
  setTimeout(function () {
    if (counter2++ < headline.length) {
      $("#top-headline").append(headline[counter2]);
      addHeadText();
    }
  }, 100);
}

$(document).ready(function () {
  $("#main").hide();
  
  setTimeout(function () {
    $("#top-name").html("");
    $("#top-headline").html("");
    addTopText();
    setTimeout(function () {
      addHeadText();
    }, 2500);
  }, 4000);

  setTimeout(function () {
    $(".top-logo").fadeIn();
    $("#top-button").fadeIn();
    $("#top-button").addClass("inline-b");
    $("#top-skip").hide();
  }, 10000);
});

$(document).ready(function () {
  $("#top-skip").click(function () {
    $("#top-skip").hide();
    $("#main").show();
    $("#screen-top").slideUp("slow");
  });
});

$(document).ready(function () {
  $("#top-button").click(function () {
    $("#main").show();
    $("#screen-top").slideUp("slow");
  });
});
