var s = [
  {
    id: "skills",
    img: "background-image: url('background/skills-min.jpg');",
    headline: "I bring CS + business together.",
    description:
      "My websites' cash flow statements would be barren if I didn't know how to integrate Google AdSense - and it wouldn't matter how many hours I spent developing an app if I never marketed it to its target audience. That's why I have worked hard to have a rich understanding of both development and business.",
    links: [
      {
        title: "Add Me On LinkedIn",
        link: "https://linkedin.com/in/cunningham1212",
      },
      {
        title: "See My Code On GitHub",
        link: "https://github.com/cunningham-code",
      },
    ],
  },
  {
    id: "venture",
    img: "background-image: url('background/venture.jpg');",
    headline: "I'm a founder and an app developer.",
    description:
      "Creating a startup requires constant iteration. The Flutter and Node.js codebases were never 'done'. Both apps I built, Preff and Niteful, taught me to keep trying new things.",
    links: [
      {
        title: "See Preff Live",
        link: "https://www.preff.menu",
      },
      {
        title: "Learn More About Niteful",
        link:
          "https://play.google.com/store/apps/details?id=com.cunningham.niteful&hl=en_US",
      },
    ],
  },
  {
    id: "industry",
    img: "background-image: url('background/industry-min.jpg');",
    headline: "I leave behind quality work.",
    description:
      "Working at both my own college and the American Red Cross taught me about the importance of legacy - the code I wrote could very well be used a decade from now. I learned that everything from marketing plans to Python programs need to be able to scale.",
  },
];

var l = [
  {
    title: "Email",
    link: "mailto:cunningjc10@gmail.com",
  },
  {
    title: "LinkedIn",
    link: "https://linkedin.com/in/cunningham1212",
  },
  {
    title: "Twitter",
    link: "https://twitter.com/cunningham_code",
  },
  {
    title: "Text",
    link: "sms:+19179701050",
  },
];
